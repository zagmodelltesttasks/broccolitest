using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class MapBuilder : MonoBehaviour
{
    public static MapBuilder instance;
    private string normalLevel = "testing_views_settings_hard_level";
    //список обьектов из файла
    public SpriteListObj mapList;
    //загруженные спрайты
    private Sprite [] levelSpritesList;
    // созданные обьекты со спрайтами
    private List<GameObject> createdMapObjects;
    //последний поставленный спрайт
    private SpriteRenderer previousSpriteRenderer;

    //для подсчета лимитов камеры
    public Vector3 upperPoint = Vector3.zero;
    public Vector3 lowerPoint = Vector3.zero;
    
    void Start()
    {
        instance = this;
        createdMapObjects = new List<GameObject>();
        levelSpritesList = Resources.LoadAll("LevelSprites", typeof(Sprite)).Cast<Sprite>().ToArray();
        BuildLevelBackground(normalLevel);
    }

    void BuildLevelBackground(string fileName)
    {
        string jSonString = Resources.Load<TextAsset>("Json/" + fileName).ToString();
        mapList = JsonUtility.FromJson<SpriteListObj>(jSonString);
        GameObject mapBase = new GameObject();
        mapBase.name = "MapBase";
        foreach (Sprite spriteObj in levelSpritesList)
        {
            foreach (MapSpriteObject mapObj in mapList.List)
            {
                if (spriteObj.name == mapObj.Id)
                {
                    GameObject newSpriteObj = new GameObject();
                    createdMapObjects.Add(newSpriteObj);
                    newSpriteObj.name = spriteObj.name;
                    newSpriteObj.transform.position = new Vector3(mapObj.X, mapObj.Y, 0f);
                    newSpriteObj.transform.parent = mapBase.transform;

                    SpriteRenderer spriteRenderer = newSpriteObj.AddComponent<SpriteRenderer>();
                    spriteRenderer.sprite = spriteObj;
                    mapList.List.Remove(mapObj);

                    //правка на размер по x, очевидно только для нашего случая в вакууме
                    if (previousSpriteRenderer != null && spriteRenderer.size.x < previousSpriteRenderer.size.x)
                    {
                        float xAdjustment = previousSpriteRenderer.size.x - spriteRenderer.size.x;
                        newSpriteObj.transform.position -= new Vector3(xAdjustment / 2, 0f, 0f);
                    }

                    //запоминаем крайнюю первую точку
                    if (upperPoint == Vector3.zero)
                    {
                        upperPoint = newSpriteObj.transform.position - new Vector3(spriteRenderer.size.x / 2, 0f, 0f) +
                                     new Vector3(0f, spriteRenderer.size.y / 2, 0f);
                    }

                    previousSpriteRenderer = spriteRenderer;
                    break;
                }
            }
        }

        //запоминаем последнюю точку
        if (previousSpriteRenderer != null)
        {
            lowerPoint = previousSpriteRenderer.transform.position +
                         new Vector3(previousSpriteRenderer.size.x / 2, 0f, 0f) -
                         new Vector3(0f, previousSpriteRenderer.size.y / 2, 0f);
        }
        System.GC.Collect();
    }

    public string FindClosestMapSprite(Vector3 measurePoint)
    {
        float closestDist = 1000f;
        GameObject closestObj = new GameObject();
        List<GameObject> tempMapSprites = createdMapObjects;
        foreach ( GameObject gObj in createdMapObjects )
        {
            float newDist = Vector3.Distance(measurePoint, gObj.transform.position);
            if (newDist < closestDist)
            {
                closestDist = newDist;
                closestObj = gObj;
            }
        }
        return closestObj.name;
    }
}
