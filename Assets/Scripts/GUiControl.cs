using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GUiControl : MonoBehaviour
{
    public static GUiControl instance;
    private GameObject menu;
    private Text menuText;
    private RectTransform buttonTransform;
    
    void Start()
    {
        instance = this;
        buttonTransform = transform.Find("Button").GetComponent<RectTransform>();
        menu = transform.Find("Menu").gameObject;
        menuText = menu.transform.Find("Text").GetComponent<Text>();
        menu.SetActive(false);
    }

    public void menuButtonClick()
    {
        if (menu.activeSelf)
        {
            InputControl.instance.SetInputActive(true);
            menu.SetActive(false);
        }
        else
        {
            InputControl.instance.SetInputActive(false);
            menu.SetActive(true);
            GetUpperCornerObjectName();
        }
    }

    void GetUpperCornerObjectName()
    {
        Vector3 basePoint = Camera.main.ScreenToWorldPoint( new Vector3( Screen.width - Screen.width *0.02f , Screen.height*0.05f, -10f));
        basePoint = new Vector3(basePoint.x, basePoint.y, 0f);
        menuText.text = MapBuilder.instance.FindClosestMapSprite(basePoint);
    }

    //хотим контролировать что не навелись на кнопку, чтобы не двигать экран
    public bool CheckButtonsAreaClick(Vector2 clickPoint)
    {
        if ( clickPoint.x < buttonTransform.position.x + buttonTransform.rect.size.x/2f
            && clickPoint.x > buttonTransform.position.x - buttonTransform.rect.size.x/2f
             && clickPoint.y > buttonTransform.position.y - buttonTransform.rect.size.y/2f
             &&clickPoint.y < buttonTransform.position.y + buttonTransform.rect.size.y/2f)
        {
            CamControl.instance.ResetPreviousCamPos();
        }
        return false;
    }

}
