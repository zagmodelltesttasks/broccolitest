using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamControl : MonoBehaviour
{
    public static CamControl instance;
    private float camLimit_Left;
    private float camLimit_Right;
    private float camLimit_Up;
    private float camLimit_Down;
    
    private float zoomLimitUp = 5f;
    private float zoomLimitDown = 3f;
    
    Vector2 prevMousePos;
    private Vector2 mapCenter;

    void Start()
    {
        RecalculateCamSize();
        instance = this;
        transform.position =  new Vector3(mapCenter.x, mapCenter.y,-10f);
        transform.position = new Vector3(mapCenter.x, mapCenter.y,-10f);
    }

    public void UpdateCamMovement()
    {
        if (prevMousePos != Vector2.zero)
        {
            Vector2 mouseDirVector = new Vector2();
            mouseDirVector = prevMousePos - new Vector2(Input.mousePosition.x, Input.mousePosition.y);
            mouseDirVector.Normalize();
            mouseDirVector = mouseDirVector * 0.3f;
            transform.position += new Vector3(mouseDirVector.x, mouseDirVector.y, 0f);
        }
        prevMousePos = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
        KeepCamLimits();
    }

    // возвращаем в пределы на всякий случай
    void KeepCamLimits()
    {
        if (transform.position.x <= camLimit_Left )
        {
            transform.position =  new Vector3(camLimit_Left +0.05f, transform.position.y,-10f);
        }
        if (transform.position.x >= camLimit_Right )
        {
            transform.position = new Vector3(camLimit_Right -0.05f, transform.position.y,-10f);
        }
        if (transform.position.y <= camLimit_Down )
        {
            transform.position = new Vector3(transform.position.x, camLimit_Down+0.05f,-10f);
        }
        if (transform.position.y >= camLimit_Up )
        {
            transform.position = new Vector3(transform.position.x, camLimit_Up-0.05f,-10f);
        }
    }

    public void ZoomCamera()
    {
        Camera.main.orthographicSize -= Input.mouseScrollDelta.y;
        if (Camera.main.orthographicSize > zoomLimitUp)
        {
            Camera.main.orthographicSize = zoomLimitUp;
        }

        if (Camera.main.orthographicSize < zoomLimitDown)
        {
            Camera.main.orthographicSize = zoomLimitDown;
        }

        RecalculateCamSize();
    }

    void RecalculateCamSize()
    {
        float camHeight = 2 * Camera.main.orthographicSize;
        float camWidth = camHeight * Camera.main.aspect;

        float mapSizeX = Vector3.Distance(new Vector3(MapBuilder.instance.upperPoint.x, 0f, 0f), new Vector3(MapBuilder.instance.lowerPoint.x, 0f, 0f));
        float mapSizeY = Vector3.Distance(new Vector3(0f, MapBuilder.instance.lowerPoint.y, 0f), new Vector3(0f, MapBuilder.instance.upperPoint.y, 0f));

        mapCenter = new Vector2(MapBuilder.instance.upperPoint.x + mapSizeX / 2, MapBuilder.instance.lowerPoint.y + mapSizeY / 2);
        
        camLimit_Left = mapCenter.x - mapSizeX/2 + camWidth/2;
        camLimit_Right = mapCenter.x + mapSizeX/2 - camWidth/2;
        camLimit_Down = mapCenter.y - mapSizeY/2 + camHeight/2;
        camLimit_Up = mapCenter.y + mapSizeY/2 - camHeight/2;
        KeepCamLimits();
    }

    public void ResetPreviousCamPos()
    {
        prevMousePos = Vector3.zero;
    }
}
