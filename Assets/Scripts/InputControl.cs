using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputControl : MonoBehaviour
{
    public static InputControl instance;
    private bool isInputActive;

    //ждем немного чтобы убедиться что зажатие а не клик
    private float startWaitTime;

    void Start()
    {
        instance = this;
        isInputActive = true;
        startWaitTime = Time.time;
    }

    void Update()
    {
        if (isInputActive)
        {
            if (Input.GetMouseButtonDown(0))
            {
                CamControl.instance.ResetPreviousCamPos();
                startWaitTime = Time.time;
            }
            GUiControl.instance.CheckButtonsAreaClick(Input.mousePosition);
            if (Input.GetMouseButton(0))
            {
                if (Time.time >= startWaitTime + 0.07f)
                {
                    CamControl.instance.UpdateCamMovement();
                }
            }
            if (Input.mouseScrollDelta.y != 0)
            {
                CamControl.instance.ZoomCamera();
            }
        }
    }

    public void SetInputActive(bool isActive)
        {
            isInputActive = isActive;
        }
}

