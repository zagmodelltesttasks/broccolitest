using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public class SpriteListObj
{
    public List<MapSpriteObject> List;
}
  
[System.Serializable]
public class MapSpriteObject
{
    public string Id;
    public string Type;
    public float Width;
    public float Height;
    public float X;
    public float Y;
}
